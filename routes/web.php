<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes(['verify'=>true]);


Route::group(['middleware' => ['auth','verified']], function () {

    Route::get('link-tree-view',['uses'=>'LinkController@manageLink']);
    Route::post('add-link',['as'=>'add.link','uses'=>'LinkController@addLink']);

    Route::get('/links', 'LinkController@index')->name('links.index');
    Route::get('/links/tableau/{link}', [
        'as' => 'tableau',
        'uses' => 'LinkController@tableau'
    ]);
    Route::resource('links', 'LinkController');


    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/components', function(){
        return view('components');
    })->name('components');


    Route::resource('users', 'UserController');

    Route::get('/profile/{user}', 'UserController@profile')->name('profile.edit');

    Route::post('/profile/{user}', 'UserController@profileUpdate')->name('profile.update');

    Route::resource('roles', 'RoleController')->except('show');


    Route::resource('category', 'CategoryController')->except('show');

    Route::resource('post', 'PostController');
    Route::resource('data', 'DataController');

    Route::get('/activity-log', 'SettingController@activity')->name('activity-log.index');

    Route::get('/settings', 'SettingController@index')->name('settings.index');

    Route::post('/settings', 'SettingController@update')->name('settings.update');
    Route::get('/userprofile', 'UserProfileController@index');
    Route::post('/userprofile', 'UserProfileController@update');


    Route::get('media', function (){
        return view('media.index');
    })->name('media.index');
});
