<?php

use Illuminate\Database\Seeder;

class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Link::class, 1)->create();
        app\Link::create([
            'link'=> 'google',
            'link_urls'=> 'https://www.youtube.com/embed/G1BcXol14u8',
            'parent_id' => '1',
            'group' => 'ProfilNasabahDormantbyInterest',
            'title' => 'DashboardDormantbyInterest',
            'type' => 2,
        ]);
        app\Link::create([
            'link'=> 'facebook',
            'link_urls'=> 'http%3A%2F%2Fdashboard.bni.co.id%2F',
            'parent_id' => '1',
            'group' => 'DashboardCrossUpSell',
            'title' => 'DashCrossUpSell',
            'type' => 1,
        ]);
        app\Link::create([
            'link'=> 'youtube',
            'link_urls'=> 'http%3A%2F%2Fdashboard.bni.co.id%2F',
            'parent_id' => '2',
            'group' => 'ProfilNasabahPeroranganBetaVersion',
            'title' => 'DashboardProfilNasabahPerorangan',
            'type' => 1,
        ]);
        app\Link::create([
            'link'=> 'chrome',
            'link_urls'=> 'http%3A%2F%2Fdashboard.bni.co.id%2F',
            'parent_id' => '2',
            'group' => 'ProfilNasabahPeroranganBetaVersion',
            'title' => 'DashboardProfilNasabahPerorangan',
            'type' => 1,
        ]);
        app\Link::create([
            'link'=> 'tiktok',
            'link_urls'=> 'http%3A%2F%2Fdashboard.bni.co.id%2F',
            'parent_id' => '3',
            'group' => 'ProfilNasabahPeroranganBetaVersion',
            'title' => 'DashboardProfilNasabahPerorangan',
            'type' => 1,
        ]);
        app\Link::create([
            'link'=> 'instagram',
            'link_urls'=> 'http%3A%2F%2Fdashboard.bni.co.id%2F',
            'parent_id' => '3',
            'group' => 'DashboardCrossUpSell',
            'title' => 'DashCrossUpSell',
            'type' => 1,
        ]);
        app\Link::create([
            'link'=> 'meta',
            'link_urls'=> 'http%3A%2F%2Fdashboard.bni.co.id%2F',
            'parent_id' => '3',
            'group' => 'ProfilNasabahDormantbyInterest',
            'title' => 'DashboardDormantbyInterest',
            'type' => 1,
        ]);
        
    }
}
