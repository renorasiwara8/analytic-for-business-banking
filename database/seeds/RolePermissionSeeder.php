<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();
        // create permissions
        $permissions = [
            'ROOT',
            'google',
            'facebook',
            'youtube',
            'chrome',
            'tiktok',
            'instagram',
            'meta',
        ];
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
        // Create Super user & role
        $admin= Role::create(['name' => 'super-admin']);
        $admin->syncPermissions($permissions);

        $usr = User::create([
            'name'=> 'Admin',
            'email' => 'admin@email.com',
            'unit' => 'adminUnit',
            'user_id' => '1',
            'npp' => 'ADMIN',
            'password' => '1',
            'status' => true,
            'email_verified_at' => now(),
        ]);

        $usr->assignRole($admin);

        $usr->syncPermissions($permissions);

        // Create user & role
        $role = Role::create(['name' => 'user']);
        // $role->givePermissionTo('update-settings');
        $role->givePermissionTo('ROOT');
        $role->givePermissionTo('google');
        $role->givePermissionTo('facebook');
        $role->givePermissionTo('youtube');
        $role->givePermissionTo('chrome');
        $role->givePermissionTo('tiktok');
        $role->givePermissionTo('instagram');
        $role->givePermissionTo('meta');

        $user = User::create([
            'name'=> 'User',
            'email' => 'user@email.com',
            'npp' => 'USER',
            'user_id' => '2',
            'password' => '2',
            'status' => true,
            'email_verified_at' => now(),
        ]);
        $user->assignRole($role);
    }
}
