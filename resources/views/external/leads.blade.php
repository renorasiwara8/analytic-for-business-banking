@extends('layouts.externalapp')
@section('content')
    <?php
        $utilDNA = new \App\DNA\UtilsDNA;
    ?>
<div class="row">
    <div class="col-md-9">
    <h2 style="color: #FFFFFF !important;">Posisi Data</h2>
        {{ Form::open() }}
        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    {{ Form::text('cif', $cif ?? "", ['class' => 'form-control form-control-sm', 'placeholder'=>'2022-07-31', 'id' => 'cif']) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
            </div>
        </div>
        {{ Form::close() }}

    </div>
</div>
<div class="card mb-0" style="background-color: #00000059;">
    <div class="card-header collapsed" data-toggle="collapse" href="#demography" style="background-color: transparent;padding-bottom: 0px;padding-top: 0px;">
        <hr style="background:grey;margin-bottom: 10px;margin-top: 10px"/>
        <a class="card-title">
            <h2 style="color: #ffa02b !important;">Leads Lending</h2>
        </a>
    </div>
    <div id="demography" class="card-body collapse" >
        <div class="row" >
            <h1 style="color: white">Under Development</h1>
        </div>
    </div>
    <div class="card-header collapsed" data-toggle="collapse" href="#funding" style="background-color: transparent;padding-bottom: 0px;padding-top: 0px;">
        <hr style="background:grey;margin-bottom: 10px;margin-top: 10px"/>
        <a class="card-title">
            <h2 style="color: #ffa02b !important;">Leads Funding</h2>
        </a>
    </div>
    <div id="funding" class="card-body collapse" >
        <div class="row" >
            <h1 style="color: white">Under Development</h1>
        </div>
    </div>
    <div class="card-header collapsed" data-toggle="collapse" href="#lending" style="background-color: transparent;padding-bottom: 0px;padding-top: 0px;">
        <hr style="background:grey;margin-bottom: 10px;margin-top: 10px"/>
        <a class="card-title">
            <h2 style="color: #ffa02b !important;">Leads Services</h2>
        </a>
    </div>
    <div id="lending" class="card-body collapse" >
        <div class="row" >
            <h1 style="color: white">Under Development</h1>
        </div>
    </div>
</div>
@endsection
