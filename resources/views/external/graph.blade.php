@extends('layouts.externalapp')
@section('content')
    <?php
        $utilDNA = new \App\DNA\UtilsDNA;
    ?>
<div class="row">
    <div class="col-md-9">
        <h2 style="color: #FFFFFF !important;">Search By :</h2>
        {{ Form::open() }}
        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    {{ Form::text('cif', $cif ?? "", ['class' => 'form-control form-control-sm', 'placeholder'=>'CIF', 'id' => 'cif']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::text('as_of_date', $as_of_date ?? "", ['class' => 'form-control form-control-sm datepicker-endmonth', 'placeholder'=>'As of Date', 'id'=>'as_of_date']) }}
                </div>
                <div class="col-md-2">
                    {!! Form::submit('Go', ['class'=> 'btn btn-primary ','style'=>'padding-bottom:5px;padding-top:5px;color:#FFFFFF;background-color:#6DA946']) !!}
                </div>
            </div>
        </div>

        <div class="form-group">

            <div class="row">




            </div>
        </div>
        {{ Form::close() }}

    </div>
</div>
<div class="card mb-0" style="background-color: #00000059;">
    <div class="card-header collapsed" data-toggle="collapse" href="#demography" style="background-color: transparent;padding-bottom: 0px;padding-top: 0px;">
        <hr style="background:grey;margin-bottom: 10px;margin-top: 10px"/>
        <a class="card-title">
            <h2 style="color: #ffa02b !important;">In Transaction</h2>
        </a>
    </div>
    <div id="demography" class="card-body collapse" >
        <div class="row" >
            <h1 style="color: white">Under Development</h1>
        </div>
    </div>
    <div class="card-header collapsed" data-toggle="collapse" href="#funding" style="background-color: transparent;padding-bottom: 0px;padding-top: 0px;">
        <hr style="background:grey;margin-bottom: 10px;margin-top: 10px"/>
        <a class="card-title">
            <h2 style="color: #ffa02b !important;">Out Transaction</h2>
        </a>
    </div>
    <div id="funding" class="card-body collapse" >
        <div class="row" >
            <h1 style="color: white">Under Development</h1>
        </div>
    </div>
    <div class="card-header collapsed" data-toggle="collapse" href="#lending" style="background-color: transparent;padding-bottom: 0px;padding-top: 0px;">
        <hr style="background:grey;margin-bottom: 10px;margin-top: 10px"/>
        <a class="card-title">
            <h2 style="color: #ffa02b !important;">Summary</h2>
        </a>
    </div>
    <div id="lending" class="card-body collapse" >
        <div class="row" >
            <h1 style="color: white">Under Development</h1>
        </div>
    </div>
    <div class="card-header collapsed" data-toggle="collapse" href="#graph" style="background-color: transparent;padding-bottom: 0px;padding-top: 0px;">
        <hr style="background:grey;margin-bottom: 10px;margin-top: 10px"/>
        <a class="card-title">
            <h2 style="color: #ffa02b !important;">Graph Transaction</h2>
        </a>
    </div>
    <div id="graph" class="card-body collapse" >
        <div class="row" >
            <h1 style="color: white">Under Development</h1>
        </div>
    </div>
</div>
@endsection
