@extends('layouts.externalapp')
@section('content')

<div class="row">

    <div class="col-md-3 text-center">

    <img src="{{asset('assets/img/logo2.png')}}" style="width:200px;heigh:200px;">

    </div>

    <div class="col-md-9">
    <h2 style="color: #FFFFFF !important;">Search By :</h2>
        {{ Form::open() }}
        <div class="form-group">
            <div class="row">

                <div class="col-md-4">
                    {{ Form::text('cif', $cif ?? "", ['class' => 'form-control form-control-sm', 'placeholder'=>'CIF', 'id' => 'cif']) }}
                </div>

            </div>
        </div>

        <div class="form-group">

            <div class="row">
                <div class="col-md-4">
                    {{ Form::text('as_of_date', $as_of_date ?? "", ['class' => 'form-control form-control-sm datepicker-endmonth', 'placeholder'=>'As of Date', 'id'=>'as_of_date']) }}
                </div>

                <div class="col-md-2">
                    {!! Form::submit('Go', ['class'=> 'btn btn-primary ','style'=>'padding-bottom:5px;padding-top:5px;color:#FFFFFF;background-color:#6DA946']) !!}
                </div>

            </div>
        </div>
        {{ Form::close() }}

    </div>
</div>
<div class="card mb-0" style="background-color: transparent;">
    @foreach($json_object->data as $key => $value)
            <div class="card-header collapsed" data-toggle="collapse" href="#{{$key}}" style="background-color: transparent;padding-bottom: 10px;padding-top: 10px;">
                <hr style="background:grey;margin-bottom: 10px;margin-top: 10px"/>
                <a class="card-title">
                    <h1 style="color: #ffa02b !important;">{{$key}}</h1>
                </a>
            </div>
            <div id="{{$key}}" class="card-body collapse" >
                <div class="row" >
                    @foreach($json_object->data->{$key} as $keyLevel1 => $valueLevel1)
                        @if(isset($json_object->data->{$key}->type))
                            @foreach($json_object->data->{$key}->{$keyLevel1} as $keyLevel2 => $valueLevel2)
                                <div class="col">
                                    <ul class="list-group">
                                        <div>
                                            <h4 class="my-0" style="color:#FFFFFF">{{$keyLevel2}}</h4>
                                            @if(str_contains($valueLevel2->value,"[") && str_contains($valueLevel2->value,"]"))
                                                <?php
                                                $item2 = str_replace("[","",$valueLevel1->value);
                                                $item2 = str_replace("]","",$item2);

                                                $item_array = explode (",", $item2);
                                                ?>
                                                @foreach($item_array as $itemTemp)
                                                    <button type="button" class="btn text-justify"
                                                            style="color:#517CC8;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">{{ trim($valueLevel2->value) == "" ? "-" : $itemTemp }}</button>
                                                @endforeach
                                            @else
                                                <button type="button" class="btn text-justify"
                                                        style="color:#517CC8;width:100%;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">{{ trim($valueLevel2->value) == "" ? "-" : $valueLevel2->value }}</button>
                                            @endif


                                        </div>
                                        <!-- </li> -->

                                    </ul>
                                </div>
                            @endforeach
                        @else
                            <div class="col">
                                <div class="row" >
                                    <div class="col">
                                        <div class="card-header collapsed" data-toggle="collapse" href="#{{$keyLevel1}}" style="background-color: transparent;padding-bottom: 10px;padding-top: 10px;">
                                            <hr style="background:grey;margin-bottom: 10px;margin-top: 10px"/>
                                            <a class="card-title">
                                                <h1 style="color: #ffa02b !important;">{{$keyLevel1}}</h1>
                                            </a>
                                        </div>
                                        <div id="{{$keyLevel1}}" class="card-body collapse" >
                                            @if(isset($valueLevel1->type))

                                            @else
                                                @foreach($json_object->data->{$key}->{$keyLevel1} as $keyLevel2 => $valueLevel2)
                                                    <div class="col">
                                                        <ul class="list-group">
                                                            <div>
                                                                <h4 class="my-0" style="color:#FFFFFF">{{$keyLevel2}}</h4>
                                                                @if(str_contains($valueLevel2->value,"[") && str_contains($valueLevel2->value,"]"))
                                                                    <?php
                                                                    $item2 = str_replace("[","",$valueLevel2->value);
                                                                    $item2 = str_replace("]","",$item2);

                                                                    $item_array = explode (",", $item2);
                                                                    ?>
                                                                    @foreach($item_array as $itemTemp)
                                                                        <button type="button" class="btn text-justify"
                                                                                style="color:#517CC8;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">{{ trim($valueLevel2->value) == "" ? "-" : $itemTemp }}</button>
                                                                    @endforeach
                                                                @else
                                                                    <button type="button" class="btn text-justify"
                                                                            style="color:#517CC8;width:100%;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">{{ trim($valueLevel2->value) == "" ? "-" : $valueLevel2->value }}</button>
                                                                @endif


                                                            </div>
                                                            <!-- </li> -->

                                                        </ul>
                                                    </div>
                                                @endforeach
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
{{--                        <?php var_dump((array)$json_object->data->{$key});die();?>--}}
{{--                        <div class="col">--}}
{{--                            <ul class="list-group">--}}
{{--                                <div>--}}
{{--                                    <h4 class="my-0" style="color:#FFFFFF">{{$key}}</h4>--}}
{{--                                    @if(str_contains($item,"[") && str_contains($item,"]"))--}}
{{--                                        <?php--}}
{{--                                        $item2 = str_replace("[","",$item);--}}
{{--                                        $item2 = str_replace("]","",$item2);--}}

{{--                                        $item_array = explode (",", $item2);--}}
{{--                                        ?>--}}
{{--                                        @foreach($item_array as $itemTemp)--}}
{{--                                            <button type="button" class="btn text-justify"--}}
{{--                                                    style="color:#517CC8;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">{{ trim($item) == "" ? "-" : $itemTemp }}</button>--}}
{{--                                        @endforeach--}}
{{--                                    @else--}}
{{--                                        <button type="button" class="btn text-justify"--}}
{{--                                                style="color:#517CC8;width:100%;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">{{ trim($item) == "" ? "-" : $item }}</button>--}}
{{--                                    @endif--}}

{{--                                </div>--}}
{{--                                <!-- </li> -->--}}

{{--                            </ul>--}}
{{--                        </div>--}}

                    @endforeach
                </div>
            </div>
    @endforeach

</div>
</div>

{{--    <hr style="background:grey;"/>--}}
{{--    <a data-toggle="collapse" href="#portfolio" role="button" aria-expanded="true" aria-controls="portfolio">--}}
{{--        <h1 style="color: #ffa02b !important;">Portfolio Product</h1>--}}
{{--    </a>--}}

{{--    <div class="row collapse multi-collapse" id="portfolio">--}}
{{--        <div class="col-md-4 order-md-2 mb-4">--}}
{{--            <ul class="list-group mb-3">--}}
{{--                <h3 class="text-center" style="color: #FFFFFF !important;">FUNDING</h3>--}}
{{--            @foreach($json_object->data->funding_currency as $key => $item)--}}
{{--                <!-- <li class="list-group-item d-flex justify-content-between lh-condensed"> -->--}}
{{--                    <div style="border-right: 2px solid gray;padding-right:30px">--}}
{{--                        <h4 class="my-0" style="color:#FFFFFF">{{$key}}</h4>--}}
{{--                        <button type="button" class="btn text-justify"--}}
{{--                                style="color:#517CC8;width:100%;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">@currency("$item")</button>--}}
{{--                    </div>--}}
{{--                    <!-- </li> -->--}}
{{--                @endforeach--}}
{{--            </ul>--}}
{{--        </div>--}}

{{--        <div class="col-md-4 order-md-2 mb-4">--}}
{{--            <ul class="list-group mb-3">--}}
{{--                <h3 class="text-center" style="color: #FFFFFF !important;">LENDING</h3>--}}
{{--            @foreach($json_object->data->loan_currency as $key => $item)--}}
{{--                <!-- <li class="list-group-item d-flex justify-content-between lh-condensed"> -->--}}
{{--                    <div>--}}
{{--                        <h4 class="my-0" style="color:#FFFFFF">{{$key}}</h4>--}}
{{--                        <button type="button" class="btn text-justify"--}}
{{--                                style="color:#517CC8;width:100%;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">@currency($item)</button>--}}
{{--                    </div>--}}
{{--                    <!-- </li> -->--}}
{{--                @endforeach--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--        <?php--}}
{{--            $dpkListName = [];--}}
{{--            $dpkList = [];--}}
{{--            $loanList = [];--}}
{{--            $loanListName = [];--}}

{{--            foreach ($json_object->data->transaction_currency as $key => $item) {--}}
{{--                if (str_contains($key, "dpk")) {--}}
{{--                    array_push($dpkList, $item);--}}
{{--                    array_push($dpkListName, $key);--}}
{{--                }--}}
{{--                if (str_contains($key, "loan")) {--}}
{{--                    array_push($loanList, $item);--}}
{{--                    array_push($loanListName, $key);--}}
{{--                }--}}
{{--            }--}}

{{--        ?>--}}
{{--        <div class="col-md-4 order-md-2 mb-4">--}}
{{--            <ul class="list-group mb-3">--}}
{{--                <h3 class="text-center" style="color: #FFFFFF !important;">TRX</h3>--}}
{{--                <div class="row">--}}
{{--                    @for($i = 0; $i < count($dpkList); $i++)--}}
{{--                        <div class="col-md-6">--}}
{{--                            <!-- <li class="list-group-item d-flex justify-content-between lh-condensed"> -->--}}
{{--                            <div style="">--}}
{{--                                <h4 class="my-0" style="color:#FFFFFF">{{$loanListName[$i]}}</h4>--}}
{{--                                <button type="button" class="btn text-justify"--}}
{{--                                        style="color:#517CC8;width:100%;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">@currency($loanList[$i])</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-6">--}}
{{--                            <!-- <li class="list-group-item d-flex justify-content-between lh-condensed"> -->--}}
{{--                            <div style="border-left: 2px solid gray;padding-left:30px">--}}
{{--                                <h4 class="my-0" style="color:#FFFFFF">{{$dpkListName[$i]}}</h4>--}}
{{--                                <button type="button" class="btn text-justify"--}}
{{--                                        style="color:#517CC8;width:100%;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">@currency($item)</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- </li> -->--}}
{{--                    @endfor--}}
{{--                </div>--}}
{{--            </ul>--}}

{{--    </div>--}}

{{--</div>--}}

{{--<hr style="background:grey;"/>--}}
{{--<a data-toggle="collapse" href="#colateral" role="button" aria-expanded="true" aria-controls="colateral"><h1 style="color: #ffa02b !important;">Colateral</h1></a>--}}
{{--<div class="row collapse multi-collapse" id="colateral" >--}}
{{--    @foreach($json_object->data->jaminan_currency as $key => $item)--}}
{{--        <div class="col-md-4 order-md-2">--}}
{{--            <ul class="list-group">--}}
{{--                <div>--}}
{{--                    <h4 class="my-0" style="color:#FFFFFF">{{$key}}</h4>--}}
{{--                    @if(str_contains($item,"[") && str_contains($item,"]"))--}}
{{--                        <?php--}}
{{--                        $item2 = str_replace("[","",$item);--}}
{{--                        $item2 = str_replace("]","",$item2);--}}

{{--                        $item_array = explode (",", $item2);--}}
{{--                        ?>--}}
{{--                        @foreach($item_array as $itemTemp)--}}

{{--                            <button type="button" class="btn text-justify"--}}
{{--                                    style="color:#517CC8;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">@currency("$item")</button>--}}
{{--                        @endforeach--}}
{{--                    @else--}}
{{--                        <button type="button" class="btn text-justify"--}}
{{--                                style="color:#517CC8;width:100%;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">@currency("$item")</button>--}}
{{--                    @endif--}}

{{--                </div>--}}
{{--                <!-- </li> -->--}}

{{--            </ul>--}}
{{--        </div>--}}

{{--    @endforeach--}}

{{--</div>--}}
{{--<hr style="background:grey;"/>--}}
{{--<a data-toggle="collapse" href="#analytics" role="button" aria-expanded="true" aria-controls="analytics"><h1 style="color: #ffa02b !important;">Analytics</h1></a>--}}
{{--<div class="row collapse multi-collapse" id="analytics" >--}}
{{--    @foreach($json_object->data->analytics as $key => $item)--}}
{{--        <div class="col-md-4 order-md-2">--}}
{{--            <ul class="list-group">--}}
{{--                <div>--}}
{{--                    <h4 class="my-0" style="color:#FFFFFF">{{$key}}</h4>--}}
{{--                    @if(str_contains($item,"[") && str_contains($item,"]"))--}}
{{--                        <?php--}}
{{--                        $item2 = str_replace("[","",$item);--}}
{{--                        $item2 = str_replace("]","",$item2);--}}

{{--                        $item_array = explode (",", $item2);--}}
{{--                        ?>--}}
{{--                        @foreach($item_array as $itemTemp)--}}
{{--                            <button type="button" class="btn text-justify"--}}
{{--                                    style="color:#517CC8;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">{{ trim($item) == "" ? "-" : $itemTemp }}</button>--}}
{{--                        @endforeach--}}
{{--                    @else--}}
{{--                        <button type="button" class="btn text-justify"--}}
{{--                                style="color:#517CC8;width:100%;padding:0.5% 0;padding-left:10px;background-color:#EEEEEE;margin-bottom:10px;">{{ trim($item) == "" ? "-" : $item }}</button>--}}
{{--                    @endif--}}

{{--                </div>--}}
{{--                <!-- </li> -->--}}

{{--            </ul>--}}
{{--        </div>--}}

{{--    @endforeach--}}

{{--</div>--}}
{{--<?php--}}
{{--$item2 = str_replace("[","",trim($json_object->data->produk->item_produk));--}}
{{--$item2 = str_replace("]","",$item2);--}}

{{--$productArray = explode (",", $item2);--}}

{{--?>--}}
{{--<hr style="background:grey;"/>--}}
{{--<a data-toggle="collapse" href="#ownership" role="button" aria-expanded="true" aria-controls="ownership">--}}
{{--    <h1 style="color: #ffa02b !important;">Product OwnerShip</h1>--}}
{{--</a>--}}
{{--    <div class="row" style="margin-bottom:20px;">--}}
{{--        <div class="col-md-12 order-md-6 mb-12 collapse multi-collapse" id="ownership">--}}
{{--                @foreach($productArray as $itemOwnership => $item)--}}
{{--                <button type="button" class="btn"--}}
{{--                        style="color:#4A473E;width:30%;margin:10px;background-color:#F6C043;">{{ $item }}</button>--}}
{{--                @endforeach--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection
