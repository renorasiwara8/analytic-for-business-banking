@extends('layouts.auth')

@section('content')

    
    <div class="row">
        <div class="col-lg-11 text-right" style="margin-top:50px">
            <img src="{{asset('assets/img/logo1.png')}}" style="width:200px;heigh:200px;">
        </div>
    </div>

    <div class="row justify-content-between" style="margin-top:10%">


        <div class="col-lg-7 text-right ">
            <div class="row">

                <div class="col-lg-4">
                    <img src="{{asset('assets/img/loginTitle.png')}}" style="width:250%;heigh:250%;margin-left:25%">
                </div>

            </div>
            
        </div>



        <div class="col-lg-4">


            <div class="col-lg-8 col-md-6">
                @include('flash::message')
            </div>
            <div class="card-body px-lg-5 py-lg-5 pt-2">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                 
                    <div class="form-group from-inline">
                        <div class="input-group input-group-merge input-group-alternative mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-check-bold"></i></span>
                            </div>
                            <input id="user_id" type="user_id" placeholder="user ID"
                                   class="form-control @error('user_id') is-invalid @enderror" name="user_id"
                                   value="{{ old('user_id') }}" required autocomplete="user_id" autofocus>

                            @error('user_id')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                            </div>
                            <input id="password" placeholder="Password" type="password"
                                   class="form-control @error('password') is-invalid @enderror" name="password" required
                                   autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror                                </div>
                    </div>
                    <!-- <div class="custom-control custom-control-alternative custom-checkbox">
                                <input class="custom-control-input" id=" customCheckLogin"  name="remember" {{ old('remember') ? 'checked' : '' }} type="checkbox">
                                <label class="custom-control-label" for=" customCheckLogin">
                                    <span>Remember me</span>
                                </label>
                            </div> -->
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mt-0">Login</button>
                    </div>
                
                </form>
    

                <div class="row mt-3">
                    <!-- <div class="col-6">
                        @if (Route::has('password.request'))
                        <a class="text-gray" href="{{ route('password.request') }}">
                                Forgot Your Password?
                            </a>


                    @endif
                    </div> -->
                    <!-- <div class="col-6 text-right">
                        @if (Route::has('register'))
                        <a class="text-gray" href="{{ route('register') }}">
                                Create new account
                            </a>


                    @endif
                    </div> -->
                </div>


            </div>


        </div>


    </div>

    <div class="row">
        <div class="col-lg-12 fixed-bottom">
        <p style="font-size:15px;color:white;">Developed by Management Data & Analytics Division
            (DMA)</p>

        </div>

  

    </div>


    <div class="row align-items-end fixed-bottom">


    </div>
    <!-- </div> -->

@endsection
