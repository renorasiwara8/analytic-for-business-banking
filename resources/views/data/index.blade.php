@extends('layouts.app')
@push('styles')

@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-5">
                <div class="card-header bg-transparent">
                    <div class="row">
                        <div class="col-lg-8">
                            <h3 class="mb-0">All Data</h3>
                        </div>
                        <div class="col-lg-4">
                    {!! Form::open(['route' => 'post.index', 'method'=>'get']) !!}
                        <div class="form-group mb-0">
                       
                    </div>

                    {!! Form::close() !!}
                </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <div>
                            <table id="jsonData" class="table table-hover align-items-center display">
                                <thead>
                                <tr>
                                @foreach($jsonExample[0] as $key=>$value)
                                    <th scope="col">{{$jsonExample[0]->$key->Alias}}</th>
                                @endforeach
                               
                        </tr>
                                </thead>
                                <tbody >

                                @foreach($jsonExample as $result)
                                    <tr>
                                    @foreach($jsonExample[0] as $a=>$b)

                                        <td>
                                            {{$result->$a->value}}
                                        </td>
                                        @endforeach

                                    </tr>
                                @endforeach
                                    

                                </tbody>
                                <tfoot >
                                <tr>
                                @foreach($jsonExample[0] as $key=>$value)
                                    <th scope="col">{{$jsonExample[0]->$key->Alias}}</th>
                                @endforeach
                                </tr>
                                
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>


 <script>
    $(document).ready(function () {
    $('#jsonData').DataTable({
        pagingType: 'simple',
        "language": {
    "paginate": {
      "previous": "<",
      "next": ">"
    }
  }
    });
});
    </script>
@endpush

