<?php
use App\Link;
$links = Link::where('parent_id', '=', 0)->get();
$allLinks = Link::pluck('link','id')->all();
$root = Link::where('parent_id', '=', 0)->first();

$user = \Auth::user();
?>
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs " id="sidenav-main" style="height: 100%;background-color: black;opacity: 1;">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  d-flex  align-items-center text-light" >
            <a style="color:white" class="navbar-brand  " href="{{ route('home') }}" data-toggle="tooltip" data-original-title="{{ setting('company_name') }}">
            {{ substr(setting('company_name'), 0, 22) }}
            </a>
            <div class=" ml-auto ">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('home*')) ? 'active' : '' }}" href="{{route('home')}}">
                            <i class="ni ni-shop text-primary"></i>
                            <span class="nav-link-text" >Dashboard</span>
                        </a>
                    </li>
                    @can('update-settings')
                        <li class="nav-item">
                            <a class="nav-link {{ (request()->is('settings*')) ? 'active' : '' }}" href="{{route('settings.index')}}">
                                <i class="ni ni-settings-gear-65 text-primary"></i>
                                <span class="nav-link-text">Manage Settings</span>
                            </a>
                        </li>
                    @endcan
                    @canany(['view-link', 'create-link'])
                        <li class="nav-item">
                            <a class="nav-link {{ (request()->is('links*')) ? 'active' : '' }}" href="{{route('links.index')}}">
                                <i class="ni ni-settings-gear-65 text-primary"></i>
                                <span class="nav-link-text">Manage Links</span>
                            </a>
                        </li>
                    @endcan

                  
                    @canany(['view-user', 'create-user'])
                        <li class="nav-item">
                            <a class="nav-link {{ (request()->is('users*')) ? 'active' : '' }}" href="#navbar-users"  data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-users">
                                <i class="fas text-primary fa-tasks"></i>
                                <span class="nav-link-text">Manage Users</span>
                            </a>
                            <div class="collapse" id="navbar-users">
                                <ul class="nav nav-sm flex-column">
                                 @can('view-user')
                                    <li class="nav-item">
                                        <a href="{{route('users.index')}}" class="nav-link"><span class="sidenav-mini-icon"></span><span class="sidenav-normal">All Users</span></a>
                                    </li>
                                    @endcan
                                    <li class="nav-item">
                                        <a href="{{route('users.create')}}" class="nav-link"><span class="sidenav-mini-icon"></span><span class="sidenav-normal">Add New User</span></a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    @endcan

                    @canany(['view-data', 'create-data'])

                        <li class="nav-item">
                            <a class="nav-link {{ (request()->is('data*')) ? 'active' : '' }}" href="#navbar-data"  data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-data">
                                <i class="fas text-primary fa-tasks"></i>
                                <span class="nav-link-text">Manage Data</span>
                            </a>
                            <div class="collapse" id="navbar-data">
                                <ul class="nav nav-sm flex-column">
                                 @can('view-data')
                                    <li class="nav-item">
                                        <a href="{{route('data.index')}}" class="nav-link"><span class="sidenav-mini-icon">D </span><span class="sidenav-normal">All Data</span></a>
                                    </li>
                                    @endcan
                                </ul>
                            </div>
                        </li>
                    @endcan

                    
                    @canany(['view-role', 'create-role'])
                        <li class="nav-item">
                            <a class="nav-link {{ (request()->is('roles*')) ? 'active' : '' }}" href="{{route('roles.index')}}">
                                <i class="fas fa-lock text-primary"></i>
                                <span class="nav-link-text">Manage Roles</span>
                            </a>
                        </li>
                    @endcan

             
                    @canany(['view-activity-log'])
                        <li class="nav-item">
                            <a class="nav-link {{ (request()->is('activity-log*')) ? 'active' : '' }}" href="{{route('activity-log.index')}}">
                                <i class="fas fa-history text-primary"></i>
                                <span class="nav-link-text">Activity Log</span>
                            </a>
                        </li>
                    @endcan

                    @if(!is_null($root))
                    <?php $counter = 0; ?>
                    @foreach($links as $link)
                    @canany([$link->link])
                        @if(count($link->childs))
                        <li class="nav-item">
                            <a class="nav-link" href="#navbar-link-parent{{ $link->link }}"  data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-link-parent{{ $link->link }}">
                                <i class="fas text-primary fa-book"></i>
                                <span class="nav-link-text">{{ $link->link }}</span>
                            </a>
                            <div class="collapse" id="navbar-link-parent{{ $link->link }}">
                                <ul class="nav nav-sm flex-column" >
                                    @include('manageChildNavBar',['childs' => $link->childs, 'level' => $counter])
                                </ul>
                            </div>
                        </li>
                        @else
                            <li class="nav-item">
                                <a href="{{route('links.show', $link)}}" class="nav-link">
                                    <i class="fas text-primary fa-link"></i>
                                    <span class="sidenav-mini-icon"></span>
                                    <span class="sidenav-normal">{{ $link->link }}</span>
                                </a>
                            </li>
                        @endif
                        <?php $counter = $counter + 1; ?>
                        @endcan
                    @endforeach
                    @endif
                 
                </ul>
            </div>
        </div>
    </div>
</nav>
