<!DOCTYPE html>
<html>
<head>
	<title>Laravel Category Treeview Example</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script src="{{asset('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('assets/css/treeview.css')}}" type="text/css">
</head>
<body>
	<div class="container">     
		<div class="panel panel-primary">
			<div class="panel-heading">Manage Link TreeView</div>
	  		<div class="panel-body">
	  			<div class="row">
	  				<div class="col-md-6">
	  					<h3>Link List</h3>
				        <ul id="tree1">
				            @foreach($links as $link)
				                <li>
				                    {{ $link->link }}
				                    @if(count($link->childs))
				                        @include('manageChild',['childs' => $link->childs])
				                    @endif
				                </li>
				            @endforeach
				        </ul>
	  				</div>
	  				<div class="col-md-6">
	  					<h3>Add New Link</h3>


				  			{!! Form::open(['route'=>'add.link']) !!}


				  				@if ($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>	
									        <strong>{{ $message }}</strong>
									</div>
								@endif


				  				<div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
									{!! Form::label('Link:') !!}
									{!! Form::text('link', old('link'), ['class'=>'form-control', 'placeholder'=>'Enter Link']) !!}
									<span class="text-danger">{{ $errors->first('link') }}</span>
								</div>

                                <div class="form-group {{ $errors->has('link_urls') ? 'has-error' : '' }}">
									{!! Form::label('Link Details:') !!}
									{!! Form::text('link_urls', old('link_urls'), ['class'=>'form-control', 'placeholder'=>'Enter Link Details']) !!}
									<span class="text-danger">{{ $errors->first('link_urls') }}</span>
								</div>


								<div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
									{!! Form::label('Link:') !!}
									{!! Form::select('parent_id',$allLinks, old('parent_id'), ['class'=>'form-control', 'placeholder'=>'Select Link']) !!}
									<span class="text-danger">{{ $errors->first('parent_id') }}</span>
								</div>


								<div class="form-group">
									<button class="btn btn-success">Add New</button>
								</div>


				  			{!! Form::close() !!}


	  				</div>
	  			</div>

	  			
	  		</div>
        </div>
    </div>
    <script src="{{asset('assets/js/treeview.js')}}"></script>
</body>
</html>