
@foreach($childs as $child)
    <?php $level = $level + 1; ?>
    @if(count($child->childs))
    <ul class="nav nav-sm flex-column">
        <li class="nav-item">
            <a class="nav-link" style="padding-left: 4.5em;" href="#navbar-link-parent{{$child->id}}"  data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-link-parent{{$child->id}}">
            <i class="fas text-primary fa-book"></i>
            <span class="nav-link-text">{{ $child->link }}</span>
            </a>
            <div class="collapse" id="navbar-link-parent{{$child->id}}">
                <ul class="nav nav-sm flex-column">
                    @include('manageChildNavBar',['childs' => $child->childs, 'level' => $level])
                </ul>
            </div>     
        </li>
    </ul>
    @else
    <ul class="nav nav-sm flex-column">
        <li class="nav-item">
        @canany([$child->link])
            <a href="{{route('links.show', $child)}}}" class="nav-link">
                <i class="fas text-primary fa-link"></i>
                <span class="sidenav-normal">{{ $child->link }}</span></a>
                @endif
        </li>
    </ul>
    @endcan

@endforeach
