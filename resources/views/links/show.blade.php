@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 p-0">
            {{ Form::open(['route' => 'settings.update', 'files'=>true])}}
            <div class="card mb-5 p-0" style="background-color: transparent">
                <div class="card-body p-0">

	  			<div class="row">
	  				<div class="col-md-12">
                      @if($link->type == 1)
                      <iframe frameBorder="0" src="{{ URL::route('tableau', $link) }}" height="700px" width="100%" title="description"></iframe>
	  				@else
                      <iframe frameBorder="0" src="{{ $link->link_urls }}" height="700px" width="100%" title="description"></iframe>
                      @endif
                    </div>

    </div>
                    </div>
            </div>
            <!-- <div class="card mb-5">
                <div class="card-header bg-transparent"><h4 class="mb-0">Display Settings</h4></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{Form::label('record_per_page', 'Record Per Page', ['class' => 'form-control-label'])}}
                                {{ Form::text('record_per_page', setting('record_per_page'), ['class'=>"form-control"])}}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {{Form::label('company_currency_symbol', 'Currency Symbol', ['class' => 'form-control-label'])}}
                                {{ Form::text('company_currency_symbol', setting('company_currency_symbol'), ['class'=>"form-control"])}}
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

            {{Form::close()}}

        </div>
    </div>
@endsection
