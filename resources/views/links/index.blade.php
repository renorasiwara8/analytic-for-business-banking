@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- {{ Form::open(['route' => 'settings.update', 'files'=>true])}} -->
            <div class="card mb-5">
                <div class="card-header bg-transparent"><h3 class="mb-0">General Links</h3></div>
                <div class="card-body">

                    <div class="container">
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <ul id="tree1">
                                            @foreach($links as $link)
                                                <li>
                                                    {{ $link->link }}
                                                    @if(count($link->childs))
                                                        @include('manageChild',['childs' => $link->childs])
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="col-md-6">
                                        <!-- <h3>Add New Link</h3> -->


                                        {!! Form::open(['route'=>'add.link']) !!}


                                        @if ($message = Session::get('success'))
                                            <div class="alert alert-success alert-block">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        @endif


                                        <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                                            {!! Form::text('link', old('link'), ['class'=>'form-control', 'placeholder'=>'Link Name']) !!}
                                            <span class="text-danger">{{ $errors->first('link') }}</span>
                                        </div>

                                        <div class="form-group {{ $errors->has('link_urls') ? 'has-error' : '' }}">
                                            {!! Form::text('link_urls', old('link_urls'), ['class'=>'form-control', 'placeholder'=>'Link Url','id'=>'link_urls']) !!}
                                            <span class="text-danger">{{ $errors->first('link_urls') }}</span>
                                        </div>

                                        <div class="form-group {{ $errors->has('group') ? 'has-error' : '' }}">
                                            {!! Form::text('group', old('group'), ['class'=>'form-control', 'placeholder'=>'link group']) !!}
                                            <span class="text-danger">{{ $errors->first('group') }}</span>
                                        </div>


                                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                            {!! Form::text('title', old('title'), ['class'=>'form-control', 'placeholder'=>'link title','id'=>'title']) !!}
                                            <span class="text-danger">{{ $errors->first('title') }}</span>
                                        </div>


                                        <div class="form-group">
                                            {{ Form::select('type', array(1 => 'Tableu', 2 => 'Non Tableu'), 1, [ 'class'=> 'selectpicker form-control', 'placeholder' => 'Select type...','id'=>'type']) }}
                                        </div>


                                        <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
                                            {!! Form::select('parent_id',$allLinks, old('parent_id'), ['class'=>'form-control', 'placeholder'=>'New Parent']) !!}
                                            <span class="text-danger">{{ $errors->first('parent_id') }}</span>
                                        </div>


                                        <div class="form-group">
                                            <button class="btn btn-success">Add New</button>
                                        </div>


                                        {!! Form::close() !!}


                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-5">
                <div class="card-header bg-transparent">
                    <div class="row">
                        <div class="col-lg-8">
                            <h3 class="mb-0">All Links</h3>
                        </div>
                        <div class="col-lg-4">
                            {!! Form::open(['route' => 'links.index', 'method'=>'get']) !!}
                            <div class="form-group mb-0">
                                {{ Form::text('search', request()->query('search'), ['class' => 'form-control form-control-sm', 'placeholder'=>'Search links']) }}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <div>
                            <table class="table table-hover align-items-center">

                                <!-- <thead class=""> -->
                                <thead class="">
                                <tr>
                                    <th scope="col">Parent</th>
                                    <th scope="col">Link Name</th>
                                    <th scope="col">Link Url</th>
                                    <th scope="col">Group</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Type</th>
                                    <th scope="col" class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody class="list">
                                @foreach($linksLists as $linksList)
                                    <tr>
                                        <th scope="row">
                                            {{$linksList->parent_name}}
                                        </th>
                                        <th scope="row">
                                            {{$linksList->link}}
                                        </th>
                                        <td class="budget">
                                            {{$linksList->link_urls}}
                                        </td>
                                        <td class="budget">
                                            {{$linksList->group}}
                                        </td>
                                        <td class="budget">
                                            {{$linksList->title}}
                                        </td>
                                        <td class="budget">
                                            {{$linksList->type}}
                                        </td>


                                        <td class="text-center">
                                            @can('destroy-link')
                                                {!! Form::open(['route' => ['links.destroy', $linksList],'method' => 'delete',  'class'=>'d-inline-block dform']) !!}
                                            @endcan

                                            @can('update-link')
                                                <a class="btn btn-info btn-sm m-1" data-toggle="tooltip"
                                                   data-placement="top" title="Edit link details"
                                                   href="{{route('links.edit',$linksList)}}">
                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                </a>
                                            @endcan
                                            @can('destroy-link')
                                                <button type="submit" class="btn delete btn-danger btn-sm m-1"
                                                        data-toggle="tooltip" data-placement="top" title="Delete link"
                                                        href="">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        jQuery(document).ready(function () {
            $('.delete').on('click', function (e) {
                e.preventDefault();
                let that = jQuery(this);
                jQuery.confirm({
                    icon: 'fas fa-wind-warning',
                    closeIcon: true,
                    title: 'Are you sure!',
                    content: 'You can not undo this action.!',
                    type: 'red',
                    typeAnimated: true,
                    buttons: {
                        confirm: function () {
                            that.parent('form').submit();
                            //$.alert('Confirmed!');
                        },
                        cancel: function () {
                            //$.alert('Canceled!');
                        }
                    }
                });
            })

            $('#type').change(function () {
                var value = $('#type').val();
                console.log(value);
                if (value != 1) {
                    // empty value and make the input editable
                    $("#link_urls").val("").prop('readonly', false);
                    $("#title").val("").prop('readonly', true);
                } else {
                    $("#link_urls").val("http%3A%2F%2Fdashboard.bni.co.id%2F").prop('readonly', true);
                    $("#title").val("").prop('readonly', false);
                }
            });

            document.getElementById('link_urls').value = 'http%3A%2F%2Fdashboard.bni.co.id%2F';
            document.getElementById('link_urls').readOnly = true;

        })

    </script>
@endpush

