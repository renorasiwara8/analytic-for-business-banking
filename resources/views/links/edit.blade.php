@extends('layouts.app')
@push('pg_btn')
    <a href="{{route('links.index')}}" class="btn btn-sm btn-neutral">All Links</a>
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-5">
                <div class="card-body">
                    @can('update-link')
                        {!! Form::open(['route' => ['links.update', $link], 'method'=>'put', 'files' => true]) !!}
                    @endcan
                    <h6 class="heading-small text-muted mb-4">Link information</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {{ Form::label('link', 'Link Name', ['class' => 'form-control-label']) }}
                                    {{ Form::text('link', $link->link, ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {{ Form::label('group', 'Group', ['class' => 'form-control-label']) }}
                                    {{ Form::text('group', $link->group, ['class' => 'form-control']) }}
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {{ Form::label('link_urls', 'Link Details', ['class' => 'form-control-label']) }}
                                    {{ Form::text('link_urls', $link->link_urls, ['class' => 'form-control','id'=>'link_urls']) }}
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    {{ Form::label('title', 'Title', ['class' => 'form-control-label']) }}
                                    {{ Form::text('title', $link->title, ['class' => 'form-control','id'=>'title']) }}
                                </div>
                            </div>

                            <div class="col-md-4">

                            </div>

                            <div class="col-md-2">

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {{ Form::label('type', 'Link Type', ['class' => 'form-control-label']) }}
                                    {{ Form::select('type', array(1 => 'Tableu', 2 => 'Non Tableu'), $link->type, [ 'class'=> 'selectpicker form-control', 'placeholder' => 'Select Parent','id'=>'type']) }}

                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    {{ Form::label('parent_id', 'Link Parent', ['class' => 'form-control-label']) }}
                                    {{ Form::select('parent_id', $links, $link->parent_id, [ 'class'=> 'selectpicker form-control', 'placeholder' => 'Select Parent']) }}
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="pl-lg-4">
                        <div class="row">

                            @can('update-link')
                                <div class="col-md-12">
                                    {{ Form::submit('Submit', ['class'=> 'mt-5 btn btn-primary']) }}
                                </div>
                            @endcan
                        </div>
                    </div>
                    @can('update-link')
                        {!! Form::close() !!}
                    @endcan
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@push('scripts')
    <script>
        jQuery(document).ready(function () {

            $('#type').change(function () {
                var value = $('#type').val();
                console.log(value);
                if (value != 1) {
                    // empty value and make the input editable
                    $("#link_urls").val("").prop('readonly', false);
                    $("#title").prop('readonly', true);
                } else {
                    $("#link_urls").val("http%3A%2F%2Fdashboard.bni.co.id%2F").prop('readonly', true);
                    $("#title").val("").prop('readonly', false);
                }
            });

        })

    </script>
@endpush
