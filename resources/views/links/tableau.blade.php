<script type='text/javascript' src='http://dashboard.bni.co.id/javascripts/api/viz_v1.js'></script>
<div class='tableauPlaceholder' style='width: 100px; height: 100px;'>
    <object class='tableauViz' width='100%' height='100%' style='display:none;'>
        <param name='host_url' value='{{ $link->link_urls }}'/>
        <param name='embed_code_version' value='3'/>
        <param name='site_root' value=''/>
        <param name='name' value='{{ $link->group }}&#47;{{ $link->title }}'/>
        <param name='tabs' value='no'/>
        <param name='toolbar' value='no'/>
        <param name='showAppBanner' value='false'/>
        <param name='filter' value='iframeSizedToWindow=true'/>
    </object>
</div>