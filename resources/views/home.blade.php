@extends('layouts.app')

@section('content')
<img src="{{asset('assets/img/pp.png')}}" style="background-repeat: no-repeat;background-size:cover;width:100%;position: absolute;margin-top:-15%;right: 0px;height: 125%;z-index:-1;">
    <div class="row">
        <div class="col-xl-7">
            <div class="p-3 mb-2 text-white" style="background-color:#0170C0">
                Profile Anda
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-xl-7">


            <div class="card">
                <div class="card-body bg-transparent">

                    <div class="row">
                        <div style="float:left;width:20%;">
                            <label>User ID</label>
                        </div>

                        <div style="float:left;width:2%;">
                            <label>:</label>
                        </div>

                        <div style="float:left;width:70%;">
                            <label>{{$user->user_id}}</label>
                        </div>
                    </div>

                    <div class="row">

                        <div style="float:left;width:20%;">
                            <label>NPP</label>
                        </div>

                        <div style="float:left;width:2%;">
                            <label>:</label>
                        </div>

                        <div style="float:left;width:70%;">
                            <label>{{$user->npp}}</label>

                        </div>

                    </div>

                    <div class="row">
                        <div style="float:left;width:20%;">
                            <label>Nama</label>
                        </div>
                        <div style="float:left;width:2%;">
                            <label>:</label>
                        </div>
                        <div style="float:left;width:70%;">
                            <label>{{$user->name}}</label>
                        </div>
                    </div>

                    <div class="row">
                        <div style="float:left;width:20%;">
                            <label>Email</label>
                        </div>
                        <div style="float:left;width:2%;">
                            <label>:</label>
                        </div>
                        <div style="float:left;width:70%;">
                            <label>{{$user->email}}</label>
                        </div>
                    </div>

                    <div class="row">
                        <div style="float:left;width:20%;">
                            <label>Unit</label>
                        </div>
                        <div style="float:left;width:2%;">
                            <label>:</label>
                        </div>
                        <div style="float:left;width:70%;">
                            <label>{{$user->unit}}</label>
                        </div>
                    </div>

                    <div class="row">
                        <div style="float:left;width:20%;">
                            <label>User</label>
                        </div>
                        <div style="float:left;width:2%;">
                            <label>:</label>
                        </div>
                        <div style="float:left;width:70%;">
                            <!-- <label>Super Admin</label> -->
                            @foreach ($user->roles as $role)
                                {{ $role->name }}
                            @endforeach
                        </div>
                    </div>


                </div>
            </div>


            <div style="color:#FFFFFF">

            <p>DNA (Data Scientist & Analytics) adalah aplikasi yang dikembangkan oleh Divisi DMA sebagai penyedia data dan informasi hasil analitik big data, machine learning, artificial intelligence, atau aktivitas lain yang relevan. DNA mencoba memastikan semua aktivitas pengumpulan data, pengolahan, analisis, serta penyajiannya memenuhi standar kualitas yang ditetapkan.</p>
            <p>Hak akses yang diberikan kepada User (berupa ID dan password) bersifat SANGAT RAHASIA serta melekat, dan setiap penggunaannya tunduk dan bertanggung jawab secara penuh pada peraturan dan ketentuan yang berlaku. Setiap User yang dengan sengaja, tanpa hak, atau melawan hukum melakukan penyalahgunaan atas data dan informasi tersebut dapat diproses sesuai hukum atau perundangan-undangan yang berlaku.</p>
            </div>
        </div>
    </div>



    <!-- <div> -->

    <!-- <div class="row">
    </div class="col-xl-12 text-right"> -->

    <!-- </div>
    </div> -->

@endsection
