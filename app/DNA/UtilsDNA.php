<?php

namespace App\DNA;
use Illuminate\Support\Facades\Http;

class UtilsDNA {
    public function getRequestProfileDebitur($cif, $asOfDate) {
        $response = Http::withHeaders([
            'vaia-key' => env('VAIA_KEY')
        ])->get(env('DEBITUR_PROFILE_URI'), [
            'cif' => $cif,
            'as_of_date' => $asOfDate
        ]);

        if ($response->getBody()) {
            $body = json_decode($response->body());
            return $body;
        }

        return false;
    }
}

