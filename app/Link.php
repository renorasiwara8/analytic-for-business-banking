<?php


namespace App;


use Illuminate\Database\Eloquent\Model;


class Link extends Model
{


    public $fillable = ['link','link_urls','parent_id','group','title','type'];


    /**
     * Get the index name for the model.
     *
     * @return string
    */
    public function childs() {
        return $this->hasMany('App\Link','parent_id','id') ;
    }
}