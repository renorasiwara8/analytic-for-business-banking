<?php


namespace App;


use Illuminate\Database\Eloquent\Model;


class RoleHasAccess extends Model
{


    public $fillable = ['access_id','role_id'];


    // /**
    //  * Get the index name for the model.
    //  *
    //  * @return string
    // */
    // public function childs() {
    //     return $this->hasMany('App\RoleHasAccess','parent_id','id') ;
    // }
}