<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests\LinkUpdateRequest;
use App\Http\Requests;
use App\Link;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;



class LinkController extends Controller
{
    public function __construct()
    {

       
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request) {
        activity('Links')
            ->causedBy(Auth::user())
            ->log('view');
        $title =  'Links';
        $roles = Role::pluck('name', 'id');
        $links = Link::where('parent_id', '=', 0)->get();
        $allLinks = Link::pluck('link','id')->all();

        $parent = Link::where('parent_id','=','0')->select('links.*','links.link as parent_name');

        if ($request->has('search')) {
            $linksLists = Link::join('links as link2','links.id','=','link2.parent_id')->select('link2.*','links.link as parent_name')->where('links.link', 'like', '%'.$request->search.'%')
            ->union($parent)
            ->paginate(setting('record_per_page', 15));
        }else{
            $linksLists= Link::join('links as link2','links.id','=','link2.parent_id')->select('link2.*','links.link as parent_name')
            ->union($parent)
            ->paginate(setting('record_per_page', 15));
        }

        return view('links.index', compact('roles', 'title','links','allLinks','linksLists','roles'));


    }

    public function manageLink()
    {
        $links = Link::where('parent_id', '=', 0)->get();
        $allLinks = Link::pluck('link','id')->all();
        return view('linkTreeview',compact('links','allLinks'));
    }

    public function show(Link $link){
        $title = 'Link Details';
        return view('links.show', compact('link','title'));
    }

    public function tableau(Link $link){
        $title = 'Tableau Details';
        return view('links.tableau', compact('link','title'));
    }

    public function destroy(Link $link)
    {
        $link->delete();

        $rLink = Link::where('parent_id', '=', $link->id)->get();
        $permission = Permission::where(['id'=>$link->id]);
        $permission->delete();

        if(!is_null($rLink)){
            $this->processDelete($rLink);
        }

       
        flash('Link deleted successfully!')->info();
        return back();
    }

    public function processDelete($rLinks){

        foreach($rLinks as $Link){
            $Link->delete();
            $rLink = Link::where('parent_id', '=', $Link->id)->get();
            $permission = Permission::where(['id'=>$Link->id]);
            $permission->delete();

            if(!is_null($rLink)){
                $this->processDelete($rLink);
            }

        }
        return true;
    }

    public function edit(Link $link)
    {
        $title = "Link Details";
        $roles = Role::pluck('name', 'id');
        $links = Link::pluck('link', 'id');
        return view('links.edit', compact('link','title', 'roles','links'));
    }
    
    public function update(LinkUpdateRequest $request, Link $link)
    {
        $input = $request->all();
        $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];
        $link->update($input);
        flash('Link updated successfully!')->success();
        return redirect()->route('links.index');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function addLink(Request $request)
    {
        $this->validate($request, [
                'link' => 'required',
        ]);
        $input = $request->all();
        $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];
        
        Link::create($input);
        Permission::create(['name'=>$request->link]);
        Role::findByName('super-admin')->givePermissionTo($request->link);

        return back()->with('success', 'New Link added successfully.');
    }


}