<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\DataRequest;
use App\Data;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;

class DataController extends Controller
{
    public function __construct()
    {

        $this->middleware('permission:view-post');
        $this->middleware('permission:create-post', ['only' => ['create','store']]);
        $this->middleware('permission:update-post', ['only' => ['edit','update']]);
        $this->middleware('permission:destroy-post', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      $utils = new DATA\UtilsDATA();
      $response = $utils->getRequestDATA("",0);

      $jsonExample = $response;
        
      $title =  'Manage Datas';
      $jsonDecode = json_decode(json_encode($jsonExample),true);
        
      return view('data.index', compact('title','jsonExample','jsonDecode'));
    }

}
