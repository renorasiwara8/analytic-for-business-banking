<?php


namespace App\Http\Controllers;


use Hamcrest\Util;
use Illuminate\Http\Request;
use App\DNA;
use App\Http\Requests\LinkUpdateRequest;
use App\Http\Requests;
use App\Link;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;



class GraphController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request) {
        return view('external.graph');
    }
}
