<?php


namespace App\Http\Controllers;


use Hamcrest\Util;
use Illuminate\Http\Request;
use App\DNA;
use App\Http\Requests\LinkUpdateRequest;
use App\Http\Requests;
use App\Link;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;



class UserProfileController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request) {
        $utils = new DNA\UtilsDNA();
        $response = $utils->getRequestProfileDebitur("",0);

        return view('external.userprofile',['json_object' =>  $response])->with("test",$response);
    }

    public function update(Request $request) {
        $utils = new DNA\UtilsDNA();
        $cif = $request->input('cif');
        $as_of_date = $request->input('as_of_date');

        $response = $utils->getRequestProfileDebitur($cif,$as_of_date);

        return view('external.userprofile',['json_object' =>  $response])->with("test",$response)->with("cif",$cif)->with("as_of_date",$as_of_date);
    }

}
