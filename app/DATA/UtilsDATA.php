<?php

namespace App\DATA;
use Illuminate\Support\Facades\Http;

class UtilsDATA {
    public function getRequestData($cif, $asOfDate) {
        $response = Http::withHeaders([
            'vaia-key' => env('DATA_VAIA_KEY')
        ])->get(env('DATA_URI'), [
            'cif' => $cif,
            'as_of_date' => $asOfDate
        ]);

        if ($response->getBody()) {
            $body = json_decode($response->body());
            return $body;
        }

        return false;
    }
}

